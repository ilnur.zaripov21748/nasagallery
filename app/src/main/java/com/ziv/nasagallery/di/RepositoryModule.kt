package com.ziv.nasagallery.di

import com.ziv.nasagallery.data.database.dao.PhotoDao
import com.ziv.nasagallery.data.network.INasaGalleryClient
import com.ziv.nasagallery.data.repository.INasaGalleryRepository
import com.ziv.nasagallery.data.repository.NasaGalleryRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideNasaGalleryRepository(
        networkClient: INasaGalleryClient,
        dbClient: PhotoDao
    ): INasaGalleryRepository = NasaGalleryRepository(networkClient, dbClient)
}