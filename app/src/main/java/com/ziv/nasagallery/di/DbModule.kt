package com.ziv.nasagallery.di

import android.content.Context
import androidx.room.Room
import com.ziv.nasagallery.data.database.AppDatabase
import com.ziv.nasagallery.data.database.dao.PhotoDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule(context: Context) {

    private val db: AppDatabase =
        Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "NASA_DB")
            .build()

    @Provides
    @Singleton
    fun providePhotoDao(): PhotoDao = db.photoDao()
}