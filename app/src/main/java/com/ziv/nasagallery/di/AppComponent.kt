package com.ziv.nasagallery.di

import com.ziv.nasagallery.ui.activity.MainActivity
import com.ziv.nasagallery.ui.fragment.gallery.GalleryFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        RepositoryModule::class,
        DbModule::class,
        NetworkModule::class,
        PresenterModule::class]
)
interface AppComponent {

    fun inject(activity: MainActivity)

    fun inject(fragment: GalleryFragment)
}