package com.ziv.nasagallery.di

import com.ziv.nasagallery.data.repository.INasaGalleryRepository
import com.ziv.nasagallery.ui.fragment.gallery.presenter.GalleryPresenter
import com.ziv.nasagallery.ui.fragment.gallery.presenter.IGalleryPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PresenterModule {

    @Provides
    @Singleton
    fun provideGalleryPresenter(iNasaGalleryRepository: INasaGalleryRepository): IGalleryPresenter =
        GalleryPresenter(iNasaGalleryRepository)
}