package com.ziv.nasagallery.di

import com.ziv.nasagallery.data.network.ClientBuilder
import com.ziv.nasagallery.data.network.INasaGalleryClient
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class NetworkModule {



    private val clientBuilder = ClientBuilder()
    private val retrofit: Retrofit

    init {
        retrofit = clientBuilder.buildRetrofit()
    }

    @Provides
    @Singleton
    fun provideNasaGalleryClient(): INasaGalleryClient =
        retrofit.create(INasaGalleryClient::class.java)
}