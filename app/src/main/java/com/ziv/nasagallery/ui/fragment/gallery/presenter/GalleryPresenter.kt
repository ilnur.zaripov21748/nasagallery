package com.ziv.nasagallery.ui.fragment.gallery.presenter

import com.ziv.nasagallery.data.repository.INasaGalleryRepository
import com.ziv.nasagallery.ui.fragment.gallery.IGalleryView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GalleryPresenter(
    val galleryRepository: INasaGalleryRepository
) : IGalleryPresenter {

    private var PAGE = 1
    private var COUNT = 20

    private var view: IGalleryView? = null

    override fun create(view: IGalleryView) {
        this.view = view
    }

    override fun destroy() {
        view = null
    }

    override fun getPhotos() {
        view?.onShowProgress()
        galleryRepository
            .getPhotos(1, 20)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ it ->
                view?.onShowPhotos(it,true)
                view?.onHideProgress()
            }, { e ->
                showError(e)
            })
    }

    override fun removePhoto(photo: Photo) {
        view?.onShowProgress()
        galleryRepository.removePhoto(photo.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ view?.onHideProgress() },
                { e ->
                    showError(e)
                })
    }

    override fun getMorePhotos() {
        view?.onShowProgress()
        PAGE++
        galleryRepository
            .getPhotos(PAGE, COUNT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ it ->
                view?.onShowPhotos(it,false)
                view?.onHideProgress()
            }, { e ->
                showError(e)
            })
    }

    private fun showError(e: Throwable) {
        view?.onHideProgress()
        view?.showError(e)
    }
}