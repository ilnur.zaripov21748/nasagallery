package com.ziv.nasagallery.ui.fragment.gallery.adapter

import com.ziv.nasagallery.ui.fragment.gallery.presenter.Photo

interface OnPhotoClickListener {
    fun onClickPhoto(photo: Photo)

    fun onDeletePhoto(photo: Photo)


}