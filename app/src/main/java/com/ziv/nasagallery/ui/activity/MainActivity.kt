package com.ziv.nasagallery.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ziv.nasagallery.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}