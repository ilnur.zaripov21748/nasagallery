package com.ziv.nasagallery.ui.fragment.photo

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.ziv.nasagallery.R
import com.ziv.nasagallery.ui.fragment.gallery.presenter.Photo

class PhotoFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val photo = arguments?.getSerializable("photo") as Photo
        Picasso.with(requireContext())
            .load(photo.download_url)
//            .centerCrop()
//            .resize((photo.width * d).toInt(), (photo.height * d).toInt())
            .into(view.findViewById<ImageView>(R.id.photo))
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_photo, container, false)
    }

}