package com.ziv.nasagallery.ui.fragment.gallery.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.ziv.nasagallery.R
import com.ziv.nasagallery.ui.fragment.gallery.presenter.Photo

class PhotoAdapter(val listener: OnPhotoClickListener) :
    RecyclerView.Adapter<PhotoAdapter.ViewHolder>() {

    private var POSITION = 0

    private var photos: MutableList<Photo> = mutableListOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(photos[position], listener)
    }


    override fun getItemCount() = photos.size

    fun replaceDataSet(photos: List<Photo>) {
        this.photos.clear()
        this.photos.addAll(photos)
        POSITION = photos.count()-1
        notifyDataSetChanged()
    }

    fun addDataSet(photos:List<Photo>){
        this.photos.addAll(photos)
        notifyItemRangeChanged(POSITION,photos.count())
        POSITION += photos.count()
    }

    private fun removePhoto(photo: Photo) {
        this.photos.remove(photo)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

//        val fio: TextView = itemView.findViewById(R.id.userListName)
//        val avatar: ImageView = itemView.findViewById(R.id.userListAvatar)
//        val countSubscriber: TextView = itemView.findViewById(R.id.userListCountSub)

        fun bind(photo: Photo, listener: OnPhotoClickListener) {
            val d = 0.3
            Picasso.with(itemView.context)
                .load(photo.download_url)
                .centerCrop()
                .resize((photo.width * d).toInt(), (photo.height * d).toInt())
                .into(itemView.findViewById<ImageView>(R.id.photo))
            itemView.setOnClickListener {
                listener.onClickPhoto(photo)
            }
            itemView.findViewById<ImageView>(R.id.button_remove_photo).setOnClickListener{
                removePhoto(photo)
                listener.onDeletePhoto(photo)
            }
        }

    }

}