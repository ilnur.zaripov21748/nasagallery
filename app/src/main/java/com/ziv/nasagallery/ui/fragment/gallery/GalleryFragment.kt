package com.ziv.nasagallery.ui.fragment.gallery

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.Button
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.ziv.nasagallery.R
import com.ziv.nasagallery.app.App
import com.ziv.nasagallery.ui.fragment.gallery.adapter.OnPhotoClickListener
import com.ziv.nasagallery.ui.fragment.gallery.adapter.PhotoAdapter
import com.ziv.nasagallery.ui.fragment.gallery.presenter.IGalleryPresenter
import com.ziv.nasagallery.ui.fragment.gallery.presenter.Photo
import javax.inject.Inject

class GalleryFragment : Fragment(), IGalleryView {

    @Inject
    lateinit var presenter: IGalleryPresenter

    private lateinit var refresh: SwipeRefreshLayout

    private var adapter: PhotoAdapter = PhotoAdapter(object : OnPhotoClickListener {
        override fun onClickPhoto(photo: Photo) {
            val arg = Bundle()
            arg.putSerializable("photo", photo)
            findNavController().navigate(R.id.action_galleryFragment_to_photoFragment, arg)
        }

        override fun onDeletePhoto(photo: Photo) {
            presenter.removePhoto(photo)
        }
    })

    init {
        App.component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.create(this)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        return inflater.inflate(R.layout.fragment_gallery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi(view)
        presenter.getPhotos()
    }

    private fun setupUi(view: View) {
        refresh = view.findViewById(R.id.refresh)
        refresh.isEnabled = false

        val viewGallery = view.findViewById<RecyclerView>(R.id.recycler_gallery)
        viewGallery.adapter = adapter

        viewGallery.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                when (newState){
                    RecyclerView.SCROLL_STATE_IDLE -> presenter.getMorePhotos()
                }
            }
        })





    }


    override fun onShowPhotos(photos: List<Photo>, isReplace: Boolean) {
        if (isReplace) adapter.replaceDataSet(photos)
        else adapter.addDataSet(photos)
    }

    override fun onShowProgress() {
        refresh.isRefreshing = true
    }

    override fun onHideProgress() {
        refresh.isRefreshing = false
    }

    override fun showError(e: Throwable) {
        Toast.makeText(this.requireContext(),e.message.toString(),Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.destroy()
    }
}