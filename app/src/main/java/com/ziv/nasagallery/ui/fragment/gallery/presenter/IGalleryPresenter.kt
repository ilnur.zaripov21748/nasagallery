package com.ziv.nasagallery.ui.fragment.gallery.presenter

import com.ziv.nasagallery.ui.fragment.gallery.IGalleryView

interface IGalleryPresenter {
    fun create(view: IGalleryView)
    fun destroy()
    fun getPhotos()
    fun removePhoto(photo: Photo)
    fun getMorePhotos()
}