package com.ziv.nasagallery.ui.fragment.gallery

import com.ziv.nasagallery.ui.fragment.gallery.presenter.Photo

interface IGalleryView {

    fun onShowPhotos(photos:List<Photo>, isReplace:Boolean)

    fun onShowProgress()
    fun onHideProgress()

    fun showError(e:Throwable)
}