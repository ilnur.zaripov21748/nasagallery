package com.ziv.nasagallery.ui.fragment.gallery.presenter

import java.io.Serializable

data class Photo (
    val id: Int,
    val author: String,
    val width: Int,
    val height: Int,
    val url: String,
    val download_url: String
        ) :Serializable