package com.ziv.nasagallery.data.network

import com.ziv.nasagallery.data.network.gallery.GalleryResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface INasaGalleryClient {

    @GET("list")
    fun getPhotos(@Query("page") page:Int, @Query("limit") count:Int): Single<GalleryResponse>

}