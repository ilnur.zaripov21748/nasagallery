package com.ziv.nasagallery.data.database.dao

import androidx.room.*
import com.ziv.nasagallery.data.database.entity.PhotoEntity
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface PhotoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhoto(photo: PhotoEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhotos(photos: List<PhotoEntity>)

    @Update
    fun updatePhoto(photo: PhotoEntity)

    @Delete
    fun deletePhoto(photo: PhotoEntity)

    @Query("SELECT * FROM PhotoEntity LIMIT :count OFFSET :page ")
    fun getAllPhotos(page: Int, count: Int): Maybe<List<PhotoEntity>>

    @Query("DELETE FROM PhotoEntity WHERE id=:id")
    fun deletePhotoById(id: Int)
}