package com.ziv.nasagallery.data.repository

import com.ziv.nasagallery.data.database.dao.PhotoDao
import com.ziv.nasagallery.data.database.entity.PhotoEntity
import com.ziv.nasagallery.data.network.INasaGalleryClient
import com.ziv.nasagallery.ui.fragment.gallery.presenter.Photo
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

class NasaGalleryRepository(
    private val networkClient: INasaGalleryClient,
    private val dbClient: PhotoDao
) : INasaGalleryRepository {

    override fun getPhotos(page: Int, count: Int): Single<List<Photo>> {
        val s1 = getPhotosFromNetwork(page, count)
            .flatMap {
                Single.zip(Single.just(it), setPhotosToDb(it),
                    { it1, _ -> it1 }
                )
            }

        val s2 = getPhotosFromDB(page, count)

        return Single
            .concat(s2, s1)
            .filter { it.isNotEmpty() }
            .firstOrError()
            .onErrorReturn { e ->
                if (e is NoSuchElementException) {
                    return@onErrorReturn emptyList<Photo>()
                }
                throw e
            }
    }

    private fun setPhotosToDb(photos: List<Photo>): Single<Boolean> {
        return Observable.just(photos)
            .flatMapIterable { it }
            .map { PhotoEntity(it.id, it.author, it.width, it.height, it.url, it.download_url) }
            .toList()
            .doOnSuccess { dbClient.insertPhotos(it) }
            .map { true }
    }

    private fun getPhotosFromNetwork(
        page: Int,
        count: Int
    ): Single<List<Photo>> {
        return networkClient.getPhotos(page, count)
            .toObservable()
            .flatMapIterable { it }
//            .filter { it.author == "NASA" }
            .map { Photo(it.id.toInt(), it.author, it.width, it.height, it.url, it.download_url) }
            .toList()
    }

    private fun getPhotosFromDB(page: Int, count: Int): Single<List<Photo>> {
        return dbClient.getAllPhotos(page * count - count, count)
            .toObservable()
            .flatMapIterable { it }
            .map { Photo(it.id, it.author, it.width, it.height, it.url, it.download_url) }
            .toList()
    }

    override fun removePhoto(id: Int): Completable {
        return Completable.fromCallable { dbClient.deletePhotoById(id) }
    }

}