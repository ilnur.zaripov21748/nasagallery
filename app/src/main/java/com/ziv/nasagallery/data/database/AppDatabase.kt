package com.ziv.nasagallery.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ziv.nasagallery.data.database.dao.PhotoDao
import com.ziv.nasagallery.data.database.entity.PhotoEntity

@Database(entities = [PhotoEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract fun  photoDao():PhotoDao
}