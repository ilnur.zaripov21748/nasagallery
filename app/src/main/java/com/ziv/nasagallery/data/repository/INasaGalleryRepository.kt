package com.ziv.nasagallery.data.repository

import com.ziv.nasagallery.ui.fragment.gallery.presenter.Photo
import io.reactivex.Completable
import io.reactivex.Single

interface INasaGalleryRepository {
    fun getPhotos(page: Int, count: Int): Single<List<Photo>>
    fun removePhoto(id: Int): Completable
}