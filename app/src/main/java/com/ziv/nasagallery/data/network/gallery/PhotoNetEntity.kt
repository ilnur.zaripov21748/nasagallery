package com.ziv.nasagallery.data.network.gallery

import com.fasterxml.jackson.annotation.JsonProperty

data class PhotoNetEntity(
    @JsonProperty("id")
    val id: String,
    @JsonProperty("author")
    val author: String,
    @JsonProperty("width")
    val width: Int,
    @JsonProperty("height")
    val height: Int,
    @JsonProperty("url")
    val url: String,
    @JsonProperty("download_url")
    val download_url: String
)