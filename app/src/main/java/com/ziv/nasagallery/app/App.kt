package com.ziv.nasagallery.app

import android.app.Application
import com.ziv.nasagallery.di.AppComponent
import com.ziv.nasagallery.di.DaggerAppComponent
import com.ziv.nasagallery.di.DbModule

class App :Application() {

    companion object{
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder()
            .dbModule(DbModule(this))
            .build()
    }
}